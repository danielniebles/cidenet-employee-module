import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import AddEmployee from '../views/Add.vue'
import EditEmployee from '../views/Edit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: { name: 'Dashboard' },
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
  },
  {
    path: '/employee/add',
    name: 'Employee',
    component: AddEmployee,
  },
  {
    path: '/employee/:id',
    name: 'EmployeeEdit',
    component: EditEmployee,
    props: true,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
