import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://localhost:8080`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})

export default {
  getEmployees() {
    return apiClient.get('/employees')
  },
  postEmployee(employee) {
    return apiClient.post('/employees/create', employee)
  },
  deleteEmployee(id) {
    return apiClient.delete('/employees/delete', { data: { _id: id } })
  },
  getEmployee(id) {
    return apiClient.get(`/employees/byId?_id=${id}`)
  },
  updateEmployee(employee) {
    return apiClient.patch('/employees/update', employee)
  },
}
