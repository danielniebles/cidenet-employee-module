import Vue from 'vue'
import Vuex from 'vuex'
import EventService from '../services/EventService'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    employees: [],
    employee: {},
  },
  mutations: {
    ADD_EMPLOYEE(state, employee) {
      state.employees.push(employee)
    },
    SET_EMPLOYEES(state, employees) {
      state.employees = employees
    },
    DELETE_EMPLOYEE(state, index) {
      state.employees.splice(index, 1)
    },
    SET_EMPLOYEE(state, employee) {
      state.employee = employee
    },
    UPDATE_EMPLOYEES(state, employee) {
      const index = state.employees.findIndex(
        (item) => item._id === employee._id
      )
      state.employees[index] = employee
    },
  },
  actions: {
    fetchEmployees({ commit }) {
      EventService.getEmployees().then((response) => {
        commit('SET_EMPLOYEES', response.data.response)
      })
    },
    createEmployee({ commit }, employee) {
      EventService.postEmployee(employee).then(() => {
        commit('ADD_EMPLOYEE', employee)
      })
    },
    deleteEmployee({ state, commit }, employee) {
      const { _id } = employee
      EventService.deleteEmployee(_id).then(() => {
        const index = state.employees.indexOf(employee)
        commit('DELETE_EMPLOYEE', index)
      })
    },
    getEmployee({ commit }, id) {
      EventService.getEmployee(id).then((response) => {
        commit('SET_EMPLOYEE', response.data.response)
      })
    },
    updateEmployee({ state, commit }, employee) {
      EventService.updateEmployee(employee).then(() => {
        commit('SET_EMPLOYEES', state.employees)
        console.log('Exitoso')
      })
    },
  },
  getters: {
    getEmployeeById: (state) => (id) => {
      console.log(state.employees)
      return state.employees.find((employee) => employee['_id'] === id)
    },
  },
  modules: {},
})
