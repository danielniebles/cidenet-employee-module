const constants = require('./constants')

const buildResponse = (res, httpCode, data, message) => {
  return res.status(httpCode).json({
    status: httpCode,
    data,
    message,
  })
}

const getDomainByCountry = (country) => {
  return country === 'COLOMBIA'
    ? `${constants.COL_DOMAIN}`
    : `${constants.US_DOMAIN}`
}

const removeWhitespaces = (string) => {
  return replaceAll(string, ' ', '')
}

const replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, 'g'), replace)
}

module.exports = { removeWhitespaces, getDomainByCountry, buildResponse }
