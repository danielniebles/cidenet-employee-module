const US_DOMAIN = 'cidenet.com.us'
const COL_DOMAIN = 'cidenet.com.co'

module.exports = { US_DOMAIN, COL_DOMAIN }
