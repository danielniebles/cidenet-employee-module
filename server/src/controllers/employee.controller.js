const express = require('express')
const router = express.Router()
const EmployeeService = require('../services/employee.service')

router.get('/', async (req, res) => {
  const response = await EmployeeService.getEmployees()
  return res.status(200).json({ response })
})

router.get('/byId', async (req, res) => {
  const response = await EmployeeService.getEmployeeById(req.query)
  return res.status(200).json({ response })
})

router.post('/create', async (req, res) => {
  const response = await EmployeeService.createEmployee(req.body)
  return res.status(201).json({ response })
})

router.patch('/update', async (req, res) => {
  const response = await EmployeeService.updateEmployee(req.body)
  return res.status(200).json({ response })
})

router.delete('/delete', async (req, res) => {
  const response = await EmployeeService.deleteEmployee(req.body)
  return res.status(200).json({ response })
})

module.exports = router
