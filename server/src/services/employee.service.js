const EmployeeModel = require('../models/employee.model')
const { removeWhitespaces, getDomainByCountry } = require('../utils/utils')

class EmployeeService {
  static async createEmployee(params) {
    const employee = new EmployeeModel()
    const properties = Object.keys(params)
    properties.forEach((property) => {
      employee[property] = params[property]
    })

    try {
      employee.email = await this.createEmployeeEmail({
        firstName: params['firstName'].toLowerCase(),
        firstLastName: removeWhitespaces(params['firstLastName']).toLowerCase(),
        domain: getDomainByCountry(params['country']),
      })
      return await employee.save()
    } catch (error) {
      console.log(error)
    }
  }

  static async getEmployees() {
    try {
      return await EmployeeModel.find({})
    } catch (error) {
      console.log(error)
    }
  }

  static async getEmployeeById({ _id }) {
    try {
      return await EmployeeModel.findOne({ _id })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateEmployee(req) {
    let { _id, ...data } = req
    let updateObject = {}

    try {
      const { firstName, firstLastName, country } =
        await EmployeeModel.findById(_id)
      if (
        firstName !== data['firstName'] ||
        firstLastName !== data['firstLastName'] ||
        country !== data['country']
      ) {
        const newEmail = await this.createEmployeeEmail({
          firstName: data['firstName'].toLowerCase(),
          firstLastName: removeWhitespaces(data['firstLastName']).toLowerCase(),
          domain: getDomainByCountry(data['country']),
        })
        updateObject = { ...data, email: newEmail }
      } else {
        updateObject = data
      }

      await EmployeeModel.updateOne(
        {
          _id,
        },
        {
          $set: {
            ...updateObject,
          },
        }
      )

      return EmployeeModel.findById(_id)
    } catch (error) {
      console.log(error)
    }
  }

  static async deleteEmployee({ _id }) {
    try {
      return await EmployeeModel.findByIdAndDelete(_id)
    } catch (error) {
      console.log(error)
    }
  }

  static async createEmployeeEmail({ firstName, firstLastName, domain }) {
    const existingEmails = await this.getExistingEmails({
      firstName,
      firstLastName,
      domain,
    })

    const email =
      existingEmails.length > 0
        ? this.getNextValidEmail({
            firstName,
            firstLastName,
            domain,
            existingEmails,
          })
        : `${firstName}.${firstLastName}@${domain}`

    return email
  }

  static async getExistingEmails({ firstName, firstLastName, domain }) {
    const regexp = `(${firstName}\.${firstLastName})(\.[0-9]{1,2})?\@${domain}`
    const response = await EmployeeModel.find({ email: { $regex: regexp } })
    const emails = response.map(({ email }) => email)
    return emails
  }

  static getNextValidEmail({
    firstName,
    firstLastName,
    domain,
    existingEmails,
  }) {
    const regexp = `(?<=${firstName}\.${firstLastName}\.)\\d+`
    const filters = []

    existingEmails.forEach((email) => {
      if (email.match(regexp) !== null) {
        filters.push(parseInt(email.match(regexp)[0]))
      }
    })

    const maxId = filters.length === 0 ? 1 : Math.max(...filters) + 1
    return `${firstName}.${firstLastName}.${maxId}@${domain}`
  }
}

module.exports = EmployeeService
