const mongoose = require('mongoose')

const EmployeeSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    otherNames: {
      type: String,
      required: false,
    },
    firstLastName: {
      type: String,
      required: true,
    },
    secondLastName: {
      type: String,
      required: false,
    },
    country: {
      type: String,
      required: true,
    },
    idType: {
      type: String,
      required: true,
    },
    idNumber: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
    },
    // enterDate: {
    //   type: Date,
    //   required: true,
    // },
    area: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    // registerTime: {
    //   type: Date,
    //   required: true,
    // },
  },
  { timestamps: true }
)

module.exports = mongoose.model('Employee', EmployeeSchema)
